#include <iostream>

int main()
{
	int a = 0;
	const int N = 10;

	while (a != N)
	{
		a++;
		int c = a;
		c = c % 2;
		if (c == 0)
		{
			std::cout << a << "\n";
		}
	}

	std::cout << "\n";

	int function(int a);
	{
		std::cout << "Enter number from 0 to 10 \n";
		std::cin >> a;

		if (a % 2 == 0)
		{
			std::cout << "it's even number";
		}

		else
		{
			std::cout << "it's odd number";
		}

		return(0);
	}
}